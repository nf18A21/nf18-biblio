


CREATE TYPE ETAT AS ENUM ('neuf', 'bon', 'abimé', 'perdu');

CREATE TABLE Contributeur(
    id_Contributeur SERIAL PRIMARY KEY NOT NULL,
    nom VARCHAR(50),
    prenom VARCHAR(50),
    date_naiss DATE,
    nationalite VARCHAR(50)
);



CREATE TABLE Film
(
code SERIAL PRIMARY KEY,
titre VARCHAR(50),
date_apparition DATE,
editeur VARCHAR(50),
genre VARCHAR(20),
code_classification INTEGER,
langue VARCHAR(50),
longueur INTEGER,
synopsis VARCHAR(600)
);

CREATE TABLE Realisateur
(
film INTEGER REFERENCES Film(code),
contributeur INTEGER REFERENCES Contributeur(id_Contributeur),
PRIMARY KEY (film, contributeur)
);

CREATE TABLE Acteur
(
film INTEGER REFERENCES Film(code),
contributeur INTEGER REFERENCES Contributeur(id_Contributeur),
    PRIMARY KEY (film, contributeur)
);



CREATE TABLE Musique (
code SERIAL PRIMARY KEY,
titre VARCHAR (50),
date_apparition DATE,
editeur VARCHAR (50),
genre VARCHAR (50),
code_classification INTEGER,
longueur INTEGER
);

CREATE TABLE Compositeur(
    musique INTEGER REFERENCES Musique (code),
    contributeur INTEGER REFERENCES Contributeur (id_Contributeur),
    PRIMARY KEY (musique, contributeur)
);

CREATE TABLE Interprete (
    musique INTEGER REFERENCES Musique (code),
    contributeur INTEGER REFERENCES Contributeur (id_Contributeur),
    PRIMARY KEY (musique, contributeur)
);

CREATE TABLE Livre(
    code SERIAL PRIMARY KEY,
    titre VARCHAR(50),
    date_apparition DATE,
    editeur VARCHAR(50),
    genre VARCHAR(50),
    code_classification INTEGER,
    ISBN VARCHAR(50),
    resume VARCHAR(600),
    langue VARCHAR(50));

CREATE TABLE Auteur(
    livre INTEGER REFERENCES Livre(code),
    contributeur INTEGER REFERENCES Contributeur(id_Contributeur),
    PRIMARY KEY (livre, contributeur)
);


CREATE TABLE Utilisateur (
    login VARCHAR PRIMARY KEY,
    password VARCHAR,
    adresse VARCHAR,
    mail VARCHAR
);

CREATE TABLE Adherent (
    login VARCHAR REFERENCES Utilisateur (login) PRIMARY KEY,
    date_birth DATE,
    num INTEGER,
    blacklist BOOL,
    suspension INTEGER
);

CREATE TABLE Administrateur(
    login VARCHAR REFERENCES Utilisateur (login) PRIMARY KEY
);



CREATE TABLE Pret
(
    id  SERIAL PRIMARY KEY,
    login VARCHAR REFERENCES Adherent(login) NOT NULL,
    id_e INTEGER REFERENCES Exemplaire(id) NOT NULL,
    date DATE,
    duree INTEGER,
    etat_retour_exemplaire ETAT,
    retour BOOL

);


CREATE TABLE Exemplaire(
    id SERIAL PRIMARY KEY,
    etat_exemplaire ETAT,
    film INTEGER REFERENCES Film(code),
    musique INTEGER REFERENCES Musique(code),
    livre INTEGER REFERENCES Livre(code)
);

/*
--INSERTION
INSERT INTO Contributeur (
'1234',
'BARBIE'
'JEAN'
TO_DATE('2003/07/09', 'yyyy/mm/dd'),
'Français',
);

INSERT INTO Musique (
'938',
'La vie est belle',
TO_DATE ('2009/09/12','yyyy/mm/dd'),
'sony',
'classique',
'2',
'3,09',
);

INSERT INTO Compositeur(
   '938',
    '1234'
);

INSERT INTO Interprete (
    '938',
    '1234',
  );

INSERT INTO Exemplaire VALUES(
    '1',
    'neuf',
    null,
    '938',
    null
)
*/