#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 13:45:26 2021

@author: bouteyoa
"""

import psycopg2

HOST = "tuxa.sme.utc"

USER = "nf18a006"

PASSWORD = "0KsIpXah"

DATABASE = "dbnf18a006"

conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (HOST, DATABASE, USER, PASSWORD))
cur = conn.cursor()


def ajouter_element(conn) : #Sous-menu 
    print("Quel élément souhaitez-vous ajouter ?\n")
    print("1 - Exemplaire d'une ressource\n")
    print("2 - Contributeur\n")
    print("3 - Adhérent\n")
    print("4 - Administrateur\n")
    print("0 - Retourner au menu principal\n")
    
    choix = input()
    
    if choix == '0' :
        return
    if choix == '1' :
        a_exemplaire(conn)
    if choix == '2' :
        a_contributeur(conn) #Le contributeur doit être tout de suite lié à une ressource
    if choix == '3' :
        a_adherent(conn)
    if choix == '4' :
        a_admin(conn)

#ajouter_element(conn)

def a_exemplaire(conn) : #Ajout d'un exemplaire dans la BDD
    nouv = input("Est-ce une nouvelle ressource ? (Y/N)\n")
    
    print("Que souhaitez-vous ajouter ?\n")
    print("1 - Un livre\n")
    print("2 - Une musique\n")
    print("3 - Un film\n")
    print("0 - Retour au menu principal\n")
    
    typeR = input()
    
    if typeR == '0':
        return
    elif typeR == '1' :
        table = 'Livre'
    elif typeR == '2' :
        table = 'Musique'
    elif typeR == '3' :
        table = 'Film'
    else :
        return
    
    if nouv == 'Y' or nouv == 'y' or nouv == 'yes' or nouv == 'Oui' or nouv == 'oui' :
        titre = input("Quel est le titre de l'exemplaire ?\n")
        dateApp = input("Quelle est la date d'apparition de la ressource ? (DD-MM-YYYY) \n")
        edit = input("Qui en est l'éditeur ?\n")
        genre = input("Dans quel genre est-elle classée ?\n")
        codeClass = input("Quel est son code de classification ?\n")
        
        if  typeR == '1' :
            isbn = input("Quel est le code ISBN du livre ?\n")
            resume = input("Indiquez le résumé du livre\n")
            langue = input("Dans quelle langue est-il écrit ?\n")
            sqlNouvRes = "INSERT INTO Livre(titre, date_apparition, editeur, genre, code_classification, ISBN, resume, langue) VALUES('%s',TO_DATE('%s', 'dd-mm-yyyy'),'%s','%s','%s','%s','%s','%s')" %(titre,dateApp,edit,genre,codeClass,isbn,resume,langue)
            sqlId = "SELECT MAX(code) FROM Livre" #L'id du livre inséré correspond au plus grand code de la table car c'est un attribut qui s'incrémente à chaque insertion
            
            cur.execute(sqlNouvRes)
            conn.commit()
            cur.execute(sqlId)
            id = cur.fetchone()[0]
            
            #Ajouter auteur (s'il doit être créé, appeler a_contributeur)
            nouvCont = input("L'auteur est-il un contributeur enregistré ? (Y/N) \n")
            if nouvCont == 'Y' or nouvCont == 'y' or nouvCont == 'yes' or nouvCont == 'Oui' or nouvCont == 'oui' :
                idCont = input("Quel est son identifiant ?\n")
                #Eventuellement, vérifier que l'auteur existe bien dans la BDD
            else :
                a_contributeurLie(conn) #Toute la partie de a_contributeur dans laquelle on défint simplement le contributeur (Il est implicitement lié à la ressource que l'on ajoute)
                sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
                cur.execute(sqlIdCont)
                idCont = cur.fetchone()[0]
            
            sqlNouvCont = "INSERT INTO Auteur VALUES((SELECT MAX(code) FROM Livre),'%s')" %(idCont) #On récupère directement l'id du livre sans requête supplémentaire
            cur.execute(sqlNouvCont)
            conn.commit()
            
        if  typeR == '2' :
            longueur = input("Quelle est la longueur en secondes de la musique ?\n")
            sqlNouvRes = "INSERT INTO Musique(titre, date_apparition, editeur, genre, code_classification, longueur) VALUES('%s',TO_DATE('%s', 'dd-mm-yyyy'),'%s','%s','%s','%s')" %(titre,dateApp,edit,genre,codeClass,longueur)
            sqlId = "SELECT MAX(code) FROM Musique"
            
            cur.execute(sqlNouvRes)
            conn.commit()
            cur.execute(sqlId)
            id = cur.fetchone()[0]
            
            nouvCont = input("Le compositeur est-il un contributeur enregistré ? (Y/N) \n")
            if nouvCont == 'Y' or nouvCont == 'y' or nouvCont == 'yes' or nouvCont == 'Oui' or nouvCont == 'oui' :
                idCont = input("Quel est son identifiant ?\n")
                #Eventuellement, vérifier que le compositeur existe bien dans la BDD
            else :
                a_contributeurLie(conn)
                sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
                cur.execute(sqlIdCont)
                idCont = cur.fetchone()[0]
            
            sqlNouvCont = "INSERT INTO Compositeur VALUES((SELECT MAX(code) FROM Musique),'%s')" %(idCont) #On récupère directement l'id du livre sans requête supplémentaire
            cur.execute(sqlNouvCont)
            conn.commit()
            
            nouvCont = input("L'interprète est-il un contributeur enregistré ? (Y/N) \n")
            if nouvCont == 'Y' or nouvCont == 'y' or nouvCont == 'yes' or nouvCont == 'Oui' or nouvCont == 'oui' :
                idCont = input("Quel est son identifiant ?\n")
            else :
                a_contributeurLie(conn)
                sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
                cur.execute(sqlIdCont)
                idCont = cur.fetchone()[0]
            
            sqlNouvCont = "INSERT INTO Interprete VALUES((SELECT MAX(code) FROM Musique),'%s')" %(idCont) #On récupère directement l'id du livre sans requête supplémentaire
            cur.execute(sqlNouvCont)
            conn.commit()
            
        if  typeR == '3' :
            langue = input("Dans quelle langue est le film ?\n")
            longueur = input("Quelle est sa longueur en minutes ?\n")
            synopsis = input("Indiquez le synopsis du film\n")
            sqlNouvRes = "INSERT INTO Film(titre, date_apparition, editeur, genre, code_classification, langue, longueur, synopsis) VALUES('%s',TO_DATE('%s', 'dd-mm-yyyy'),'%s','%s','%s','%s','%s','%s')" %(titre,dateApp,edit,genre,codeClass, langue, longueur, synopsis)
            sqlId = "SELECT MAX(code) FROM Film" 
            
            cur.execute(sqlNouvRes)
            conn.commit()
            cur.execute(sqlId)
            id = cur.fetchone()[0]
            
            nouvCont = input("Le réalisateur est-il un contributeur enregistré ? (Y/N) \n")
            if nouvCont == 'Y' or nouvCont == 'y' or nouvCont == 'yes' or nouvCont == 'Oui' or nouvCont == 'oui' :
                idCont = input("Quel est son identifiant ?\n")
            else :
                a_contributeurLie(conn)
                sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
                cur.execute(sqlIdCont)
                idCont = cur.fetchone()[0]
            
            sqlNouvCont = "INSERT INTO Realisateur VALUES((SELECT MAX(code) FROM Film),'%s')" %(idCont) #On récupère directement l'id du livre sans requête supplémentaire
            cur.execute(sqlNouvCont)
            conn.commit()
            
            nouvCont = input("L'acteur principal est-il un contributeur enregistré ? (Y/N) \n")
            if nouvCont == 'Y' or nouvCont == 'y' or nouvCont == 'yes' or nouvCont == 'Oui' or nouvCont == 'oui' :
                idCont = input("Quel est son identifiant ?\n")
            else :
                a_contributeurLie(conn)
                sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
                cur.execute(sqlIdCont)
                idCont = cur.fetchone()[0]
            
            sqlNouvCont = "INSERT INTO Acteur VALUES((SELECT MAX(code) FROM Film),'%s')" %(idCont) #On récupère directement l'id du livre sans requête supplémentaire
            cur.execute(sqlNouvCont)
            conn.commit()
            
    elif nouv == 'N' or nouv == 'n' or nouv =='no' or nouv == 'Non' or nouv == 'non' :
        id = input("Quel est l'identifiant de la ressource ?\n")
        
        sql = "SELECT COUNT(*) FROM %s WHERE Code = '%s'" %(table,id)
        cur.execute(sql)
        res = cur.fetchone()[0]
    
        if res == 0 :
            print("Erreur : ressource inexistante")
            return
        
    else : 
        return   
    
    sqlInsert = "INSERT INTO Exemplaire(etat_exemplaire, %s) VALUES('neuf', '%s')" %(table,id) #Un exemplaire est considéré par défaut comme neuf à sa création
    cur.execute(sqlInsert)
    conn.commit()
    print("Exemplaire ajouté avec succès")
    return
        
def a_contributeur(conn): #Créer un contributeur et l'ajouter à une ou plusieurs ressources existantes
    nom = input("Quel est le nom de famille du contributeur ?\n")
    prenom = input("Quel est son prénom ?\n")
    dateN = input("Quand est-il né ? (dd-mm-yyyy)\n")
    nat = input("Quelle est sa nationalité ?\n")
    
    nbRess = input("A combien d'oeuvres a-t-il contribué ?\n")
    
    if nbRess == '0' :
        print("Erreur : Un contributeur doit être lié à une ressource existante")
        return
    
    sqlNouvCont = "INSERT INTO Contributeur(nom,prenom,date_naiss,nationalite) VALUES('%s','%s',TO_DATE('%s', 'dd-mm-yyyy'),'%s')" %(nom,prenom,dateN,nat)
    cur.execute(sqlNouvCont)
    conn.commit()
    sqlIdCont = "SELECT MAX(id_contributeur) FROM Contributeur"
    cur.execute(sqlIdCont)
    idCont = cur.fetchone()[0]
    
    for i in range(1,int(nbRess)+1) :
        numOeuvre = "Oeuvre n°%s :\n" %(i) 
        print(numOeuvre)
        print("Le contributeur est-il :\n")
        print("1 - Un auteur de livre ?\n")
        print("2 - Un interprète de musique ?\n")
        print("3 - Un compositeur de musique ?\n")
        print("4 - Un acteur de film ?\n")
        print("5 - Un réalisateur de film ?\n")
        
        natureCont = input()
        if natureCont == '1' :
            idLivre = input("Quel est l'identifiant du livre ?\n")
            sqlTest = "SELECT COUNT(*) FROM Livre WHERE code = '%s'" %(idLivre)
            cur.execute(sqlTest)
            test = cur.fetchone()[0]
            
            if test == 0 :
                print("Cette ressouce n'existe pas\n")
            else :
                sqlInsert = "INSERT INTO Auteur VALUES('%s','%s')" %(idLivre, idCont)
                cur.execute(sqlInsert)
                conn.commit()
                print("Réponse enregistrée\n")
        elif natureCont == '2' :
            idMusique = input("Quel est l'identifiant de la musique ?\n")
            sqlTest = "SELECT COUNT(*) FROM Musique WHERE code = '%s'" %(idMusique)
            cur.execute(sqlTest)
            test = cur.fetchone()[0]
            
            if test == 0 :
                print("Cette ressouce n'existe pas\n")
            else :
                sqlInsert = "INSERT INTO Interprete VALUES('%s','%s')" %(idMusique, idCont)
                cur.execute(sqlInsert)
                conn.commit()
                print("Réponse enregistrée\n")
        elif natureCont == '3' :
            idMusique = input("Quel est l'identifiant de la musique ?\n")
            sqlTest = "SELECT COUNT(*) FROM Musique WHERE code = '%s'" %(idMusique)
            cur.execute(sqlTest)
            test = cur.fetchone()[0]
            
            if test == 0 :
                print("Cette ressouce n'existe pas\n")
            else :
                sqlInsert = "INSERT INTO Compositeur VALUES('%s','%s')" %(idMusique, idCont)
                cur.execute(sqlInsert)
                conn.commit()
                print("Réponse enregistrée\n")
        elif natureCont == '4' :
            idFilm = input("Quel est l'identifiant du film ?\n")
            sqlTest = "SELECT COUNT(*) FROM Film WHERE code = '%s'" %(idFilm)
            cur.execute(sqlTest)
            test = cur.fetchone()[0]
            
            if test == 0 :
                print("Cette ressouce n'existe pas\n")
            else :
                sqlInsert = "INSERT INTO Acteur VALUES('%s','%s')" %(idFilm, idCont)
                cur.execute(sqlInsert)
                conn.commit()
                print("Réponse enregistrée\n")
        elif natureCont == '5' :
            idFilm = input("Quel est l'identifiant du film ?\n")
            sqlTest = "SELECT COUNT(*) FROM Film WHERE code = '%s'" %(idFilm)
            cur.execute(sqlTest)
            test = cur.fetchone()[0]
            
            if test == 0 :
                print("Cette ressouce n'existe pas\n")
            else :
                sqlInsert = "INSERT INTO Realisateur VALUES('%s','%s')" %(idFilm, idCont)
                cur.execute(sqlInsert)
                conn.commit()
                print("Réponse enregistrée\n")
    return

def a_contributeurLie(conn) : #Créer un contributeur qui est déjà lié par défaut à une ressource (que l'on crée) ->Il suffit juste d'insérer le contributeur dans la BDD
    nom = input("Quel est le nom de famille du contributeur ?\n")
    prenom = input("Quel est son prénom ?\n")
    dateN = input("Quand est-il né ? (dd-mm-yyyy)\n")
    nat = input("Quelle est sa nationalité ?\n")
    
    sqlNouvCont = "INSERT INTO Contributeur(nom,prenom,date_naiss,nationalite) VALUES('%s','%s',TO_DATE('%s', 'dd-mm-yyyy'),'%s')" %(nom,prenom,dateN,nat)
    cur.execute(sqlNouvCont)
    conn.commit()
        
    return


ajouter_element(conn)
    
    
    
    
    
    
    