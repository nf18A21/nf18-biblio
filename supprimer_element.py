#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 13:43:00 2021

@author: blancedo
"""
import psycopg2


HOST="tuxa.sme.utc"

USER="nf18a006"

PASSWORD="0KsIpXah"

DATABASE="dbnf18a006"

conn=psycopg2.connect("host=%s dbname=%s user=%s password=%s" %(HOST, DATABASE, USER, PASSWORD))
cur=conn.cursor()



def supprimer_element(conn) :
    print ("Quel élément voulez-vous supprimer?\n" )
    print ("1 - Exemplaire d'une ressource \n")
    print ("2 - Contributeur \n")
    print ("3 - Adhérent \n")
    print ("4 - Administrateur\n")
    print ("0 retourner au menu principal")
    
    choix = input ()
    
    if choix == '1':
        s_exemplaire(conn)
    if choix == '2':
        s_contributeur(conn)
    if choix == '3':
        s_adhérent(conn)
    if choix == '4':
        s_administrateur(conn)
    if choix== '0' :
        return 
    
def s_exemplaire(conn):
   
    print ("Quel est le type de ressources à supprimer?")
    print("Tapez ")
    print ("-1 : Pour supprimer un livre")
    print ("-2 : Pour supprimer un film")
    print ("-3 : Pour supprimer une musique")
    
    #vérification
  
    #sql2 = "SELECT film FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
    #sql3 = 
   
    #cur.execute(sql2)
    #b = cur.fetchone()
    a = input()
    
    print ("Quel est l'id de l'exemplaire?")
    id = input()
    
    if a == '1':
        sql1 = "SELECT livre FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute (sql1)
        b = cur.fetchone()
        sql_livre ="SELECT count(*) FROM EXEMPLAIRE where EXEMPLAIRE.livre='%s'" %(b)
        sql_livre_1= "DELETE FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute(sql_livre_1)
        conn.commit()
        
        cur.execute(sql_livre)
        livre_1=cur.fetchone()
        
        if livre_1[0] == 0  :
            sql_contributeur_1 = "SELECT CONTRIBUTEUR FROM AUTEUR WHERE livre = '%s'" %(b)
            cur.execute (sql_contributeur_1)
            for c in cur.fetchall() :
                print(c[0])
                i = 0
                sql_contributeur_3 = "DELETE FROM AUTEUR WHERE AUTEUR.livre = '%s'" %(b)
                cur.execute(sql_contributeur_3)
                conn.commit()
                for name in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
                    sql_contributeur_2 = "SELECT COUNT(*) FROM %s WHERE CONTRIBUTEUR = '%s' " %(name, c[0])
                    cur.execute(sql_contributeur_2)
                    if cur.fetchone()[0] > 0:
                        i = 1
                
                if i == 0 :
                    sql_contributeur_4 = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(c[0])
                    cur.execute(sql_contributeur_4)
                    conn.commit()
                        
                    
            sql_livre_2= "DELETE FROM LIVRE WHERE LIVRE.code = '%s'" %(b)
            cur.execute(sql_livre_2)
            conn.commit()
    
    if a == '2' :
        sql1 = "SELECT film FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute(sql1)
        b = cur.fetchone()[0]
        
        sql_film = "SELECT count(*) FROM EXEMPLAIRE where EXEMPLAIRE.film='%s'" %(b)
        sql_film_1 = "DELETE FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute(sql_film_1)
        conn.commit()
        
        cur.execute(sql_film)
        film_1=cur.fetchone()
        
        if film_1[0] == 0 :
            sql_contributeur_1 = "SELECT CONTRIBUTEUR FROM ACTEUR WHERE film= '%s'" %(b)
            cur.execute (sql_contributeur_1)
            for c in cur.fetchall ():
                print(c[0])
                i = 0
                sql_contributeur_3 = "DELETE FROM ACTEUR WHERE ACTEUR.film = '%s'" %(b)
                cur.execute(sql_contributeur_3)
                conn.commit()
                for name in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
                    sql_contributeur_2 = "SELECT COUNT(*) FROM %s WHERE CONTRIBUTEUR = '%s' " %(name, c[0])
                    cur.execute(sql_contributeur_2)
                    if cur.fetchone()[0] > 0:
                        i =1
                
                
                if i == 0:
                    sql_contributeur_4 = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(c[0])
                    cur.execute(sql_contributeur_4)
                    conn.commit()
                    
            sql_contributeur_1 = "SELECT CONTRIBUTEUR FROM REALISATEUR WHERE film= '%s'" %(b)
            cur.execute (sql_contributeur_1)
            for c in cur.fetchall ():
                print(c[0])
                i = 0
                sql_contributeur_3 = "DELETE FROM REALISATEUR WHERE REALISATEUR.film = '%s'" %(b)
                cur.execute(sql_contributeur_3)
                conn.commit()
                for name in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
                    sql_contributeur_2 = "SELECT COUNT(*) FROM %s WHERE CONTRIBUTEUR = '%s' " %(name, c[0])
                    cur.execute(sql_contributeur_2)
                    if cur.fetchone()[0] > 0:
                        i =1
                
                
                if i == 0:
                    sql_contributeur_4 = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(c[0])
                    cur.execute(sql_contributeur_4)
                    conn.commit()
                
        sql_film_2= "DELETE FROM FILM WHERE FILM.code = '%s'" %(b)
        cur.execute(sql_film_2)
        conn.commit()
                
                        
        
        
        
                        
            
            
    if a == '3' :
        sql1 = "SELECT musique FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute(sql1)
        b = cur.fetchone()[0]
        
        sql_musique = "SELECT count(*) FROM EXEMPLAIRE where EXEMPLAIRE.musique='%s'" %(b)
        sql_musique_1 = "DELETE FROM EXEMPLAIRE WHERE EXEMPLAIRE.id = '%s'" %(id)
        cur.execute(sql_musique_1)
        conn.commit()
        
        cur.execute(sql_musique)
        musique_1=cur.fetchone()
        
        if musique_1[0] == 0 :
            sql_contributeur_1 = "SELECT CONTRIBUTEUR FROM INTERPRETE WHERE musique= '%s'" %(b)
            cur.execute (sql_contributeur_1)
            for c in cur.fetchall ():
                print(c[0])
                i = 0
                sql_contributeur_3 = "DELETE FROM INTERPRETE WHERE INTERPRETE.musique = '%s'" %(b)
                cur.execute(sql_contributeur_3)
                conn.commit()
                for name in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
                    sql_contributeur_2 = "SELECT COUNT(*) FROM %s WHERE CONTRIBUTEUR = '%s' " %(name, c[0])
                    cur.execute(sql_contributeur_2)
                    if cur.fetchone()[0] > 0:
                        i =1
                
                
                if i == 0:
                    sql_contributeur_4 = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(c[0])
                    cur.execute(sql_contributeur_4)
                    conn.commit()
                    
            sql_contributeur_1 = "SELECT CONTRIBUTEUR FROM COMPOSITEUR WHERE musique= '%s'" %(b)
            cur.execute (sql_contributeur_1)
            for c in cur.fetchall ():
                print(c[0])
                i = 0
                sql_contributeur_3 = "DELETE FROM COMPOSITEUR WHERE COMPOSITEUR.musique = '%s'" %(b)
                cur.execute(sql_contributeur_3)
                conn.commit()
                for name in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
                    sql_contributeur_2 = "SELECT COUNT(*) FROM %s WHERE CONTRIBUTEUR = '%s' " %(name, c[0])
                    cur.execute(sql_contributeur_2)
                    if cur.fetchone()[0] > 0:
                        i =1
                
                
                if i == 0:
                    sql_contributeur_4 = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(c[0])
                    cur.execute(sql_contributeur_4)
                    conn.commit()
                
        sql_film_2= "DELETE FROM musique WHERE musique.code = '%s'" %(b)
        cur.execute(sql_film_2)
        conn.commit()
        

def s_contributeur(conn) :
    
    print("Quel est l'id du contributeur à supprimer?")
    id = input()
    i = 0
    for ressources in ["auteur" , "compositeur", "realisateur", "acteur", "interprete"]:
        sql_contributeur = "SELECT COUNT(*) FROM %s WHERE CONTRIBUETEUR = %s" %(name, id)
        cur.execute(sql_contributeur)
        if cur.fetchone() [0]>0:
            i = 1
    if i == 0:
        sql_contributeur = "DELETE FROM CONTRIBUTEUR WHERE CONTRIBUTEUR.id_contributeur = '%s'" %(id)
        cur.execute(sql_contributeur)
        conn.commit()
        
def s_adhérent(conn):
    print("Quel est le login de l'adhérent à supprimer?")
    login = input()
    sql_adhérent = " SELECT COUNT(*) FROM PRET WHERE login = %s" %(login)
    cur.execute(sql_adhérent)
    if cur.fetchone() [0] == 0:
        sql_adhérent = "DELETE FROM ADHERENT WHERE ADHERENT.login = %s" %(login)
        cur.execute(sql_adhérent)
        conn.commit()
    else:
        print("L'adhérent a des prêts en cours !!")
        
def s_administrateur(conn):
    print("Quel est le login de l'adhérent à supprimer?")
    login = input()
    sql_administrateur = "DELETE FROM ADMINISTRATEUR WHERE ADIMINISTRATEUR.login=%s" %(s)
    cur.execute(sql_administrateur)
    cur.commit     

supprimer_element(conn) 
        
        
        
        
        
        
        
        
        
        
    
  
    
